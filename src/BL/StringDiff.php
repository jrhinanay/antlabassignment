<?php

class StringDiff{

    const UNMODIFIED = 0;
    const DELETED    = 1;
    const INSERTED   = 2;

    public static function index(Response $response){
        $response->status(200);
        $response->render("StringDiff/view.php", []);
    }

    public static function differentiate(Request $request, Response $response){
        $strOne = $request->getBody()['stringOne'];
        $strTwo = $request->getBody()['stringTwo'];

        $variables = array(
            'stringOneResult' => self::renderFirstString(self::compare($strOne,$strTwo)),
            'stringTwoResult' => self::renderSecondString(self::compare($strOne,$strTwo)),
            'request'=>$request->getBody(),
        );

        $response->status(200);
        $response->render("StringDiff/view.php", $variables);
    }

    private static function compare($strOne, $strTwo, $compareCharacters=true) : array{
        $start = 0;
        $sequence1 = $strOne;
        $sequence2 = $strTwo;
        $end1 = strlen($strOne) - 1;
        $end2 = strlen($strTwo) - 1;

        // skip any common prefix
        while ($start <= $end1 && $start <= $end2
            && $sequence1[$start] == $sequence2[$start]){
            $start ++;
        }

        // skip any common suffix
        while ($end1 >= $start && $end2 >= $start
            && $sequence1[$end1] == $sequence2[$end2]){
            $end1 --;
            $end2 --;
        }

        // compute the table of longest common subsequence lengths
        $table = self::computeTable($sequence1, $sequence2, $start, $end1, $end2);

        // generate the partial diff
        $partialDiff =
            self::generatePartialDiff($table, $sequence1, $sequence2, $start);

        // generate the full diff
        $diff = array();
        for ($index = 0; $index < $start; $index ++){
            $diff[] = array($sequence1[$index], self::UNMODIFIED);
        }
        while (count($partialDiff) > 0) $diff[] = array_pop($partialDiff);
        for ($index = $end1 + 1;
             $index < ($compareCharacters ? strlen($sequence1) : count($sequence1));
             $index ++){
            $diff[] = array($sequence1[$index], self::UNMODIFIED);
        }
        return $diff;
    }

    private static function computeTable($sequence1, $sequence2, $start, $end1, $end2) : array {
        // determine the lengths to be compared
        $length1 = $end1 - $start + 1;
        $length2 = $end2 - $start + 1;

        // initialise the table
        $table = array(array_fill(0, $length2 + 1, 0));

        // loop over the rows
        for ($index1 = 1; $index1 <= $length1; $index1 ++){

            // create the new row
            $table[$index1] = array(0);

            // loop over the columns
            for ($index2 = 1; $index2 <= $length2; $index2 ++){

                // store the longest common subsequence length
                if ($sequence1[$index1 + $start - 1]
                    == $sequence2[$index2 + $start - 1]){
                    $table[$index1][$index2] = $table[$index1 - 1][$index2 - 1] + 1;
                }else{
                    $table[$index1][$index2] =
                        max($table[$index1 - 1][$index2], $table[$index1][$index2 - 1]);
                }
            }
        }
        return $table;
    }

    private static function generatePartialDiff($table, $sequence1, $sequence2, $start) : array {
        //  initialise the diff
        $diff = array();

        // initialise the indices
        $index1 = count($table) - 1;
        $index2 = count($table[0]) - 1;

        // loop until there are no items remaining in either sequence
        while ($index1 > 0 || $index2 > 0){

            // check what has happened to the items at these indices
            if ($index1 > 0 && $index2 > 0
                && $sequence1[$index1 + $start - 1]
                == $sequence2[$index2 + $start - 1]){

                // update the diff and the indices
                $diff[] = array($sequence1[$index1 + $start - 1], self::UNMODIFIED);
                $index1 --;
                $index2 --;

            }elseif ($index2 > 0
                && $table[$index1][$index2] == $table[$index1][$index2 - 1]){

                // update the diff and the indices
                $diff[] = array($sequence2[$index2 + $start - 1], self::INSERTED);
                $index2 --;

            }else{

                // update the diff and the indices
                $diff[] = array($sequence1[$index1 + $start - 1], self::DELETED);
                $index1 --;
            }
        }
        return $diff;
    }

    private static function renderFirstString($diff) : string {
        $html = '';
        foreach ($diff as $letter) {
            if($letter[1]==self::DELETED){
                $html.= '<mark style="color:white; background-color:#009d00">' .$letter[0].'</mark>';
            }
            else if($letter[1]==self::UNMODIFIED)
                $html .= $letter[0];
        }
        return $html;
    }

    private static function renderSecondString($diff) : string {
        $html = '';
        foreach ($diff as $letter){
            if($letter[1]==self::INSERTED)
                $html.= '<mark style="color:white; background-color:#fd1818">' .$letter[0].'</mark>';
            else if($letter[1]==self::UNMODIFIED)
                $html.=$letter[0];
        }
        return $html;
    }
}