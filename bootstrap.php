<?php

require_once(dirname(__FILE__) . "/config/default.php");

require_once(dirname(__FILE__) . "/resources/library/Router.php");
require_once(dirname(__FILE__) . "/resources/library/Request.php");
require_once(dirname(__FILE__) . "/resources/library/Response.php");

require_once(dirname(__FILE__) . "/src/BL/StringDiff.php");