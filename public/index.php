<?php
require_once dirname(__FILE__) . '/../bootstrap.php';

Router::get('/', function (Request $req, Response $res) {
    StringDiff::index($res);
});
Router::post('/', function (Request $req, Response $res) {
    StringDiff::differentiate($req, $res);
});