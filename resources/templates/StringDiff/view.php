<h2>String Differentiator</h2>
<div>
    <form action="./" method="post">
        <label for="stringOne">String One:</label><br>
        <textarea id="stringOne" name="stringOne" rows="6" maxlength="2000" cols="50"><?php echo ($request['stringOne'] ?? ''); ?></textarea><br>
        <label for="stringTwo">String Two:</label><br>
        <textarea id="stringTwo" name="stringTwo" rows="6" maxlength="2000" cols="50"><?php echo ($request['stringTwo'] ?? ''); ?></textarea><br><br>
        <input class="success" id="btnDiffSubmit" type="submit" name="Submit" value="Check String Diff" />
    </form>
</div>

<?php if(!empty($stringOneResult) && !empty($stringTwoResult)): ?>
<div id="divDiffResult">
    <table>
        <thead>
        <tr>
            <td>String One</td>
            <td>String Two</td>
            <td>Comparison</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?php echo ($request['stringOne'] ?? ''); ?></td>
            <td><?php echo ($request['stringTwo'] ?? ''); ?></td>
            <td><?php echo '<p>'.$stringOneResult.'</p>'
                    .'<p>'. $stringTwoResult.'</p>'; ?></td>
        </tr>
        </tbody>
    </table>
</div>
<?php endif; ?>