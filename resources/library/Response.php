<?php
class Response
{
    private int $status = 200;

    public function status(int $code) : Response{
        $this->status = $code;
        return $this;
    }

    public function render($contentFile, $variables = array()) {
        $contentFileFullPath = TEMPLATES_PATH . "/" . $contentFile;

        if (count($variables) > 0)
            foreach ($variables as $key => $value)
                if (strlen($key) > 0)
                    ${$key} = $value;

        require_once(TEMPLATES_PATH . "/header.php");

        echo "<div id=\"container\">\n"
            . "\t<div id=\"content\">\n";

        require_once($contentFileFullPath);

        echo "\t</div>\n";

        require_once(TEMPLATES_PATH . "/footer.php");
    }
}