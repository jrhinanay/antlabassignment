<?php

class Request
{
    public array $params;
    public string $reqMethod;

    public function __construct($params = []) {
        $this->params = $params;
        $this->reqMethod = trim($_SERVER['REQUEST_METHOD']);
    }

    public function getBody() : array {
        if ($this->reqMethod !== 'POST')
            return [];

        $body = [];
        foreach ($_POST as $key => $value)
            $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);

        return $body;
    }
}